package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    int max_size;
    ArrayList<FridgeItem> FridgeArray;
    
    public Fridge(){
    FridgeArray = new ArrayList<FridgeItem>(20);
    max_size = 20;
    }
    
    @Override
    public int nItemsInFridge() {
        int i = 0;
        for (FridgeItem item : FridgeArray){
            if (item.getName()!= null){
            i++;
            }
        }
        return i;
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if ((totalSize()-nItemsInFridge()) > 0 == true){
            FridgeArray.add(item);
            return ((totalSize()-nItemsInFridge()) >= 0);
        }
        else{
        return ((totalSize()-nItemsInFridge()) > 0);
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (FridgeArray.contains(item) == false){
            throw new NoSuchElementException();
        }  
        else{
            FridgeArray.remove(item);
        }
    }

    @Override
    public void emptyFridge(){
        FridgeArray.clear(); 
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> Expired_items = new ArrayList<FridgeItem>();
        for (FridgeItem item : FridgeArray){
            if (item.hasExpired()==true){
                Expired_items.add(item);
            }
        }
        FridgeArray.removeAll(Expired_items);
        return Expired_items;
    }

    public void run() {
    }


    
}
